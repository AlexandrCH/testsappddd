﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TestsApp.BLL;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(TestsApp.IoC.NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof(TestsApp.IoC.NinjectWebCommon), "Stop")]

namespace TestsApp.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        { 
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            JobScheduler.Start();
        }
    }
}
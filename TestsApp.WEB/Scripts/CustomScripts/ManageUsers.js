﻿var url = "/api/BlockUsersApi";
var urlUnblock = "/api/UnblockUsersApi";
var urlCheck = "/api/CheckUserStatus";
var urlManageUser = "/Administration/ManageUsers";
var urlUserRoles = "/Administration/GetUserRoles/";
var urlEditUser = "/Administration/EditUsersProfile/";
//var urlGetUsers = "/Tests/GetUsers";
var urlGetUsers = "/api/GetUsers";
var urlGetCurrentUser = "/Administration/GetCurrentUserId";
var name;

var Manage = function () {
    var self = this;
    self.user = ko.observable();
    self.Users = ko.observableArray();
    self.Roles = ko.observableArray();
    self.RolesAdmin = ko.observableArray();
    self.CurrentUserId = ko.observable();
    self.Test = ko.observable(false);
    var tempArray = [];
    self.InitUserRoles = function (ManageRolesModel) {
        if (ManageRolesModel) {
            var mappedRoles = $.map(ManageRolesModel.Roles, function (item) {
                return {
                    Selected: item.Selected, Value: item.Value, Disabled: item.Disabled
                }
            });
            self.Roles(mappedRoles);
            tempArray = [];
            for (var i = 0; i < self.Roles().length; i++) {
                if (self.Roles()[i].Value != "Administrator") {
                    tempArray.push(self.Roles()[i]);
                }
            }
            self.RolesAdmin(tempArray);
        }
    }

    self.InitUsers = function (model) {
        if (model) {
            var mappedUsers = $.map(model, function (item) {
                return {
                    Id: item.Id, UserName: item.UserName
                }
            });
            self.Users(mappedUsers);
        }
    }

    self.Save = function () {
        var SelectedRoles = [];
        $.each(self.Roles(), function (index, value) {
            if (value.Selected==true) {
                SelectedRoles.push(value.Value);
            }
        });

        $.ajax(urlManageUser, {
            data: ko.toJSON({
                Id: self.user().Id,
                Roles: self.Roles,
                SelectedRoles: SelectedRoles,
                UserName: self.user().UserName
            }),

            type: "POST", contentType: "application/json",
            success: function (data, status, xhr) {
                alertify.alert("", "Roles are updated");
            },
            error: function (xhr, status, error) {
                alertify.alert(xhr.status)
            }
        });
    };

    self.GetRoles = function () {
        name = self.user().UserName;

        $.ajax(urlUserRoles + self.user().Id, {

            type: "GET", contentType: "application/json",
            success: function (data, status, xhr) {
                self.InitUserRoles(data);
                $(".content").show();
                $(".loadAnimation").hide();
                if (self.CurrentUserId() == self.user().Id) {
                    self.Test(true);
                    $("#boolHelper").text("true");
                }
                else {
                    self.Test(false);
                    $("#boolHelper").text("false");
                }
            },
            error: function (xhr, status, error) {
                alertify.alert(xhr.status)
            }
        });
    };


    self.goToURL = function () {
        location.href = urlEditUser + self.user().Id;
    }

    self.GetCurrentUserId = function () {
        $.ajax(urlGetCurrentUser, {
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            success: function (data, status) {
                self.CurrentUserId(data);
            },
            error: function (error) {
                alert(error.status + error.statusText);
            }
        })
    }

    self.GetUsers = function () {
        $.ajax(urlGetUsers, {
            type: "GET",
            contentType: 'application/json',
            success: function (data, status, xhr) {
                self.InitUsers(data);
            },
            error: function (xhr, status, error) {
                alert(xhr + '\n' + status + '\n' + error);
            }
        });
    }
};

$(function () {
    $(".content").hide();
    var model = new Manage();
    model.GetUsers();
    model.GetCurrentUserId();
    ko.applyBindings(model);
});


//Block/Unblock user area

function Unblock() {
    $.ajax({
        url: urlUnblock,
        type: "POST",
        data: JSON.stringify(name),
        contentType: "application/json",
        success: function (data, status, xhr) {
            $("#result").text(xhr.responseText);
        }
    });
}

function Block() {
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(name),
        contentType: "application/json",
        success: function (data, status, xhr) {
            $("#result").text(xhr.responseText);
        }
    });
}

$(function () {
    $('#toggleBlockUnblock').change(function () {

        if ($(this).prop('checked')) {
            Unblock();
        }
        else
            Block();
    });
});

function FindUser() {
    $.ajax({
        url: urlCheck,
        type: "POST",
        data: JSON.stringify(name),
        contentType: "application/json",
        success: function (data, status, xhr) {
            if (xhr.status == 200) {
                $('#toggleBlockUnblock').prop('checked', false).change();
            }
            if (xhr.status == 202) {
                $('#toggleBlockUnblock').prop('checked', true).change();
            }
        }
    });
}

function CheckIfSelectedRoleAdmin() {
    var t = $('#boolHelper').text();
    if (t == "true") {
        $('#allRoles').hide();
        $('#rolesForAdmin').show();
    }
    else {
        $('#rolesForAdmin').hide();
        $('#allRoles').show();
    }
}
setInterval(function () {
    CheckIfSelectedRoleAdmin();
}, 1000);

FindUser()
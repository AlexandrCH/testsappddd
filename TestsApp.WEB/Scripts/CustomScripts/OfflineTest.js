﻿var urlOffLineTest = "/Tests/OfflineTest";
var urlGetTest = "/Tests/GetTest/";
var urlGetAllTests = "/Tests/GetAllTests";
var urlGetUsers = "/Tests/GetUsers";

var OfflineTest = function () {
    var self = this;
    self.test = ko.observable();
    self.user = ko.observable();
    self.Tests = ko.observableArray();
    self.Users = ko.observableArray();
    self.Questions = ko.observableArray();
    self.IsChecked = ko.observableArray();

    self.InitQuestions = function (TestModel) {
        if (TestModel) {
            var mappedQuestions = $.map(TestModel.Questions, function (item) {
                return {
                    Id: item.Id, QuestionName: item.QuestionName, Answers: ko.observableArray(item.Answers)
                }
            });
            self.Questions(mappedQuestions);
        }
    }

    self.Initialize = function (model) {
        if (model) {
            var mappedTests = $.map(model, function (item) {
                return {
                    Id: item.Id, TestName: item.TestName, Questions: ko.observableArray(item.Questions)
                }
            });
            self.Tests(mappedTests);
        }
    }

    self.InitUsers = function (model) {
        if (model) {
            var mappedUsers = $.map(model, function (item) {
                return {
                    Id: item.Id, UserName: item.UserName
                }
            });
            self.Users(mappedUsers);
        }
    }

    self.Save = function () {
        $.ajax(urlOffLineTest, {
            data: ko.toJSON({
                TestId: self.test().Id,
                ApplicationUserId: self.user().Id,
                Questions: self.Questions
            }),

            type: "POST", contentType: "application/json",
            success: function (data, status, xhr) {
                //alert(status + " ok")
                window.location = "/Tests/Index"
            },
            error: function (error) {
                alert(error.responseText + " Error")
            }
        });
    };

    self.Send = function () {
        $.ajax(urlGetTest + self.test().Id, {

            type: "GET", contentType: "application/json",
            success: function (data, status) {
                self.InitQuestions(data);
            },
            error: function (error) {
                alert(error.responseText + " Error")
            }
        });
    };

    self.GetTests = function () {
        $.ajax(urlGetAllTests, {
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },

            success: function (data, status) {
                self.Initialize(data);
            },
            error: function (error) {
                alert(error.status + error.statusText);
            },
        });
    }

    self.GetUsers = function () {
        $.ajax(urlGetUsers, {
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },

            success: function (data, status) {
                self.InitUsers(data);
            },
            error: function (error) {
                alert(error.status + error.statusText);
            }
        });
    }
};

$(function () {
    $('#loading').hide();
    var model = new OfflineTest();
    model.GetTests();
    model.GetUsers();
    ko.applyBindings(model);

    $('#print').click(function () {
        $('#userForm').hide();
        $('#buttonsArea').hide();
        window.print();
        location.reload();
    })
});

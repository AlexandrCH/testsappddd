﻿var Test = function () {

    var self = this;
    self.TestName = ko.observable();
    self.Information = ko.observable();

    self.Questions = ko.observableArray();

    self.addQuestion = function () {
        self.Questions.push({
            QuestionName: "",
            Answers: ko.observableArray()
        });
    };

    self.removeQuestion = function (question) {
        self.Questions.remove(question);
    };

    self.addAnswer = function (answer) {
        answer.Answers.push({
            AnswerName: "",
            IsCorrect: ko.observable()
        });
    };

    self.removeAnswer = function (answer) {
        $.each(self.Questions(), function () { this.Answers.remove(answer) })
    };

    self.save = function () {
       var test = $("#testDropDown").val();
       var info = $("#infoDropDown").val();

       if (test == "" || info == "") {
           alert("'Test Name' and 'Information' are required!");
       }
       else {
           $.ajax("/Tests/CreateTest", {
               data: ko.toJSON({
                   TestName: self.TestName,
                   Information: self.Information,
                   Questions: self.Questions
               }),
               type: "POST", contentType: "application/json",
               beforeSend: function () {
                   $('#loading').show();
               },
               complete: function () {
                   $('#loading').hide();
               },
               success: function (data, status, xhr) {
                   //alert(status)
                   window.location = "/Tests/Index"
               }
           });

       }
    };
};


$(function () {
    ko.applyBindings(new Test());
    $("#addQuestion").click(function () {
        $("#test_Container").attr('hidden', false);
    })
    $('#loading').hide();
});


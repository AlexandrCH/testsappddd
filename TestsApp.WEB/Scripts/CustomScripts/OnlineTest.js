﻿var urlOnlineTest = "/Tests/OnlineTest";
var urlAllTests = "/Tests/Index";
var urlGetCurrentUser = "/Tests/GetCurrentUser";
var urlGetTest = "/Tests/GetTestById/";

var boolHelper = true;
var TestViewModel = function () {
    var self = this;
    self.TestName = ko.observableArray();
    self.Questions = ko.observableArray();
    self.TempQuestions = ko.observableArray();
    self.CurrentUser = ko.observable();
    self.TestId = ko.observable();

    self.InitQuestions = function (TestModel) {
        if (TestModel) {
            self.TestName(TestModel.TestName);
            var mappedQuestions = $.map(TestModel.Questions, function (item) {

                var mappedAnswer = $.map(item.Answers, function (x) {
                    var count = [];
                    for (var i = 0; i < item.Answers.length; i++) {
                        if (item.Answers[i].IsCorrect) {
                            count.push(item.Answers[i]);
                        }
                    }
                    return {
                        AnswerName: x.AnswerName, IsCorrect: x.IsCorrect, Count: count, IsChecked: x.IsChecked
                    }
                })
                return {
                    Id: item.Id, QuestionName: item.QuestionName, Answers: ko.observableArray(mappedAnswer)
                }
            });

            self.Questions(mappedQuestions);
            self.ShowQuestion();
        }
    }

    self.EnableSaveButton = function () {
        if (self.Questions().length == self.TempQuestions().length) {
            $("#saveResult").attr('disabled', false);
            $("#nextQuestion").attr('disabled', true);
            $("#nextButton").attr('hidden', true);
            $("#saveButton").attr('hidden', false);
            if (self.Questions().length != 0) {
                boolHelper = false;
            }
        }
    }

    var i = 0;
    self.ShowQuestion = function () {
        if (i < self.Questions().length) {
            self.TempQuestions.push(self.Questions()[i++]);
        }
        self.EnableSaveButton();
    }

    self.Save = function () {
        $.ajax(urlOnlineTest, {
            data: ko.toJSON({
                TestId: self.TestId,
                ApplicationUserId: self.CurrentUser,
                Questions: self.TempQuestions
            }),

            type: "POST", contentType: "application/json",
            success: function (data, status, xhr) {
                window.location = urlAllTests
            },
            error: function (error) {
                alert(error.responseText + " Error")
            }
        })
            .done(function () {
                $('#loading').hide();
            });
    };

    self.GetCurrentUser = function () {
        $.ajax(urlGetCurrentUser, {
            type: "GET",
            contentType: 'application/json; charset=utf-8',

            success: function (data, status) {
                self.CurrentUser(data);
            },
            error: function (error) {
                alert(error.status + error.statusText);
            }
        })
    }

    self.SelectAnswer = function () {
        $("#nextQuestion").attr('disabled', false);
    }

    self.DontKnowAnswer = function () {
        var temp = $("input.answerCheckBox");
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].disabled == false) {
                temp[i].checked = false;
            }
        }
        $("#nextQuestion").attr('disabled', false);
        $("input[type=checkbox]").attr('disabled', true);
        $("input[type=radio]").attr('disabled', true);
    }

    self.GetTestById = function () {
        var url = (window.location).href;
        var id = url.substring(url.lastIndexOf('/') + 1);
        self.TestId(id);
        $.ajax(urlGetTest + id, {
            type: "GET",
            contentType: 'application/json; charset=utf-8',

            success: function (data, status) {
                self.InitQuestions(data);
                $(".loadAnimation").hide();
                $(".content").show();
            },
            error: function (error) {
                alert(error.status + " <--and--> " + error.statusText);
            }
        })
            .done(function () {
                $('#loading').hide();
                var time = 60 * 10,
                display = $('#counter');
                startTimer(time, display);
            });
    }
};

$(function () {
    var model = new TestViewModel();
    model.GetTestById();
    model.GetCurrentUser();
    model.ShowQuestion();
    ko.applyBindings(model);

    $("#saveResult").attr('disabled', true);
    $("#nextQuestion").attr('disabled', true);
    $("#nextButton").attr('hidden', false);
    $("#saveButton").attr('hidden', true);
    $(function () {
        $("#nextQuestion").click(function () {
            $("#nextQuestion").attr('disabled', true);
        })
    })

    //Counter();

    //var time = 60 * 10,
    //display = $('#counter');
    //startTimer(time, display);

    DisableCheckBoxes();
})

$(function () {
    $("#saveResult").click(function () {
        $(".content").hide();
        $(".loadAnimation").show();
    });
});


var time;

function Counter() {
    time = 300;
    var counter = $('#counter');
    counter.text("You have " + time + " seconds to answer on questions");
    setInterval(function () {
        time--;
        if (time > 0) {
            counter.text("You have " + time + " seconds to answer on questions");
        }
        if (time == 0) {
            temp = $("input");

            counter.text("");
            var dontKnow = $("input.dontKnowCheckbox");
            for (var i = 0; i < dontKnow.length; i++) {
                if (dontKnow[i].disabled == false) {
                    dontKnow[i].checked = true;
                }
            }
            var answers = $("input.answerCheckBox");
            for (var i = 0; i < answers.length; i++) {
                if (answers[i].disabled == false) {
                    answers[i].checked = false;
                }
            }
            $("input").attr('disabled', true);
            $("#nextQuestion").trigger("click");
            if (boolHelper == true) {
                Counter();
            }
            else {
                counter.text("Click on 'Save Results' button");
                time = 0;
            }
        }
    }, 1000);
}


function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text("You have " + minutes + ":" + seconds + " to answer on questions");

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}


var temp;

$("#nextQuestion").mouseover(function () {
    temp = $("input");
});

function DisableCheckBoxes() {
    $("#nextQuestion").click(function () {
        for (var i = 0; i < temp.length; i++) {
            temp[i].disabled = true;
        }
        time = 30;
    })
}
﻿var urlManage = "/api/ChangeProfile";
var url = "/Account/EditProfileGetData";

var EditProfile = function () {
    var self = this;
    self.FullName = ko.observable();
    self.Email = ko.observable();
    self.Id = ko.observable();
    self.Name = ko.observable();
    self.Roles = ko.observableArray();
    self.TestResults = ko.observableArray();
    self.Display = ko.observable();
    self.Initialize = function (EditProfileModel) {
        if (EditProfileModel) {
            self.FullName(EditProfileModel.FullName);
            self.Email(EditProfileModel.Email);
            self.Id(EditProfileModel.Id);
            self.Name(EditProfileModel.Name);

            var mappedRoles = $.map(EditProfileModel.Roles, function (item) {
                return {
                    Role: item
                }
            });
            self.Roles(mappedRoles);
            var mappedTestResults = $.map(EditProfileModel.Results, function (t) {
                return {
                    Result: t.Result,
                    Date: t.Date,
                    TestName: t.Test.TestName
                }
            });
            self.TestResults(mappedTestResults);

            if (self.TestResults().length == 0) {
                self.Display(false);
            }
            else {
                self.Display(true);
            }
        }
    }

    self.GetData = function () {
        $.ajax(url, {
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            success: function (data, status, xhr) {
                self.Initialize(data);
                $(".content").show();
                $(".loadAnimation").hide();
            },
            error: function (xhr, status, error) {
                alert(error.status + " <--and--> " + error.statusText);
            }
        });
    }
    self.SaveData = function () {
        $.ajaxSetup({
            contentType: "application/json",
            data: ko.toJSON({
                FullName: self.FullName,
                Id: self.Id,
            })
        });
        $.post(urlManage, 
        function (data, status, xhr) {
            alertify.alert("", xhr.responseText);

        });
        $(document).ajaxError(function (event, xhr, options, exc) {
            alertify.alert("", xhr.responseText);
        });
    }
}

$(function () {
    $(".content").hide();
    var model = new EditProfile();
    model.GetData();
    ko.applyBindings(model);
});
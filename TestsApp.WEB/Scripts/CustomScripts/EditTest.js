﻿var Test = function () {

    var self = this;
    self.TestName = ko.observable();
    self.Information = ko.observable();
    self.Id = ko.observable();
    self.Questions = ko.observableArray();

    self.Initialize = function (TestModel) {
        if (TestModel) {
            self.TestName(TestModel.TestName);
            self.Information(TestModel.Information);
            self.Id(TestModel.Id);

            var mappedQuestions = $.map(TestModel.Questions, function (item) {
                return {
                    Id: item.Id, QuestionName: item.QuestionName, Answers: ko.observableArray(item.Answers)
                }
            });

            self.Questions(mappedQuestions);
        }
    }

    self.addQuestion = function () {
        self.Questions.push({
            Id: "",
            QuestionName: "",
            Answers: ko.observableArray()
        });
    };

    self.removeQuestion = function (question) {
        self.Questions.remove(question);
    };

    self.addAnswer = function (answer) {
        answer.Answers.push({
            AnswerName: "",
            IsCorrect: ko.observable()
        });
    };

    self.removeAnswer = function (answer) {
        $.each(self.Questions(), function () { this.Answers.remove(answer) })
    };

    self.save = function () {
        $.ajax("/Tests/EditTest", {
            data: ko.toJSON({
                Id: self.Id,
                TestName: self.TestName,
                Information : self.Information,
                Questions: self.Questions
            }),
            type: "POST", contentType: "application/json",
            beforeSend: function(){
                $('#loading').show();
            },
            complete: function(){
                $('#loading').hide();
            },
            success: function (data, status, xhr) {
                window.location = "/Tests/Index"
            }
        });
    };

    self.GetTests = function () {
        var url = (window.location).href;
        var id = url.substring(url.lastIndexOf('/') + 1);
        $.ajax("/Tests/GetTest/"+ id, {
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            success: function (data, status) {
                self.Initialize(data);
            },
            error: function (error) {
                alert(error.status + " <--and--> " + error.statusText);
            }
        });
    }
};

$(function () {
    var model = new Test();
    model.GetTests();
    ko.applyBindings(model);
    $("#test_Container").attr('hidden', false);
    $('#loading').hide();
});


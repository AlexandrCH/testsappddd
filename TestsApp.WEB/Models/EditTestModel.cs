﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestsApp.Core.Entities;

namespace TestsApp.WEB.Models
{
    public class EditTestModel
    {
        public int Id { get; set; }
        public string TestName { get; set; }
        public string Information { get; set; }
        public ICollection<Question> Questions { get; set; }
    }
}
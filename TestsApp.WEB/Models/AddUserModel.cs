﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TestsApp.WEB.Models
{
    public class AddUserModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string FullName { get; set; }

        public string RegistrationDate { get; set; }

        public SelectList Roles { get; set; }

        public string Role { get; set; }
    }
}
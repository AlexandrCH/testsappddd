﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TestsApp.WEB.Models
{
    public class ManageRolesModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Id { get; set; }

        [Required]
        public IList<SelectListItem> Roles { get; set; }

        [Required]
        public string[] SelectedRoles { get; set; }
    }
}
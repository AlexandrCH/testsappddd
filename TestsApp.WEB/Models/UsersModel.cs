﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestsApp.WEB.Models
{
    public class UsersModel
    {
        public SelectList Users { get; set; }

        public string User { get; set; }
        public IEnumerable<SelectListItem> UsersTest { get; set; }
    }
}
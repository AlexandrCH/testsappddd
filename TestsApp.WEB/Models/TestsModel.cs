﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestsApp.Core.Entities;

namespace TestsApp.WEB.Models
{
    public class TestsModel
    {
        public int Id { get; set; }
        public SelectList Tests { get; set; }

        public string TestName { get; set; }

        public IEnumerable<Test> AllTests { get; set; }
        public string UserId { get; set; }
    }
}
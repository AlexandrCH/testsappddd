﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TestsApp.Core.Entities;

namespace TestsApp.WEB.Models
{
    public class EditProfileModel
    {
        public string Id { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string FullName { get; set; }

        public List<string> Roles { get; set; }

        public ICollection<TestResult> Results { get; set; }
    }
}
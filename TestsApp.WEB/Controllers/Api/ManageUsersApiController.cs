﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestsApp.Core;
using TestsApp.WEB.Models;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using TestsApp.Core.Interfaces;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity.EntityFramework;
using TestsApp.Core.Entities;
using System.Web.Http.Description;
using System.Web.Http.Cors;

namespace TestsApp.WEB.Controllers.Api
{
    [EnableCors("*", "*", "GET,POST")]
    [Authorize]
    public class ManageUsersApiController : ApiController
    {
        #region Constants
        private const string SameValue = "It is the same value!";
        private const string NameChanged = "FullName is changed!!!";
        private const string Empty = "Empty field!";
        private const string DataNull = "Data is Null";
        private const string UserBlocked = "User is blocked";
        private const string UserUnbocked = "User is unblocked";
        private const string NotFoundUser = "User not found";
        private const string ProfileChanged = "Profile is changed!!!";
        private const string PermissionChanged = "!!! Your permissions are changed by Administrator! You will be logged out!";

        #endregion
        private IUserService UserService;
        private ISignalR signalR;
        private IManagerIdentity ManagerIdentity;
        private ITestService TestService;

        public ManageUsersApiController(IUserService UserService, ISignalR signalR, IManagerIdentity ManagerIdentity, ITestService TestService)
        {
            this.UserService = UserService;
            this.signalR = signalR;
            this.ManagerIdentity = ManagerIdentity;
            this.TestService = TestService;
        }

        [HttpPost]
        [Route("api/ChangeProfile")]
        public async Task<HttpResponseMessage> Modify(EditProfileModel data)
        {
            if (!string.IsNullOrEmpty(data.FullName))
            {
                var user = await UserService.FindByIdAsync(data.Id);
                if (user != null)
                {
                    if (user.FullName == data.FullName)
                        return new HttpResponseMessage(HttpStatusCode.BadGateway) { Content = new StringContent(SameValue) };
                    user.FullName = data.FullName;
                    var result = await UserService.UpdateUserAsync(user);
                    if (result.Succeeded)
                    {
                        return new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent(NameChanged) };
                    }
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(Empty) };
                }
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(DataNull) };
        }

        [Authorize(Roles = ApplicationRoles.Administrator)]
        [HttpPost]
        [Route("api/CheckUserStatus")]
        public async Task<HttpResponseMessage> Check([FromBody] string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found!");
            }
            var user = await UserService.FindByNameAsync(value);
            if (user != null)
            {
                if (user.LockoutEndDateUtc == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.Accepted);
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            return new HttpResponseMessage(HttpStatusCode.BadGateway);
        }

        [Authorize(Roles = ApplicationRoles.Administrator)]
        [HttpPost]
        [Route("api/BlockUsersApi")]
        public async Task<HttpResponseMessage> Block([FromBody] string value)
        {
            var user = await UserService.FindByNameAsync(value);

            if (user != null)
            {
                user.LockoutEndDateUtc = DateTime.Now.AddYears(1);
                await UserService.UpdateUserAsync(user);
                signalR.NotifyUserAndLogout(value, PermissionChanged);
                return new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent(UserBlocked) };
            }
            return new HttpResponseMessage(HttpStatusCode.BadGateway);
        }

        [Authorize(Roles = ApplicationRoles.Administrator)]
        [HttpPost]
        [Route("api/UnblockUsersApi")]
        public async Task<HttpResponseMessage> UnBlock([FromBody] string value)
        {
            var user = await UserService.FindByNameAsync(value);

            if (user != null)
            {
                user.LockoutEnabled = true;
                user.LockoutEndDateUtc = null;
                await UserService.UpdateUserAsync(user);
                return new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent(UserUnbocked) };
            }
            return new HttpResponseMessage(HttpStatusCode.BadGateway);
        }

        [Authorize(Roles = ApplicationRoles.Administrator)]
        [HttpPost]
        [Route("api/ChangeUserProf")]
        public async Task<HttpResponseMessage> ChangeUserProfile(EditProfileModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadGateway, ModelState);
            }
            var user = await UserService.FindByIdAsync(data.Id);
            if (user != null)
            {
                if (user.FullName == data.FullName)
                    return new HttpResponseMessage(HttpStatusCode.BadGateway) { Content = new StringContent(SameValue) };

                user.FullName = data.FullName;
                user.UserName = data.Name;
                await UserService.UpdateUserAsync(user);

                return new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent(ProfileChanged) };
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, NotFoundUser);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/GetUsers")]
        public IHttpActionResult GetAppUsers()
        {
            var users = ManagerIdentity.GetUsers();
            if (users != null)
            {
                return Json(users, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            }
            return NotFound();
        }

       
    }
}
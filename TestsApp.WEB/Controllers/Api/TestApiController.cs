﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestsApp.Core;
using TestsApp.WEB.Models;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using TestsApp.Core.Interfaces;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity.EntityFramework;
using TestsApp.Core.Entities;
using System.Web.Http.Description;
using System.Web.Http.Cors;
using System.Security.Claims;
using System.Linq;
using Microsoft.Owin.Security;
using System.Web;

namespace TestsApp.WEB.Controllers.Api
{
    [EnableCors("*","*","GET,POST")]
    public class TestApiController : ApiController
    {
        #region Constants
        private const string SameValue = "It is the same value!";
        private const string NameChanged = "FullName is changed!!!";
        private const string Empty = "Empty field!";
        private const string DataNull = "Data is Null";
        private const string UserBlocked = "User is blocked";
        private const string UserUnbocked = "User is unblocked";
        private const string NotFoundUser = "User not found";
        private const string _EditTest = "Test is changed!!!";
        private const string PermissionChanged = "!!! Your permissions are changed by Administrator! You will be logged out!";

        #endregion
        private ITestService TestService;
        private IUserService UserService;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }
        public TestApiController(ITestService TestService, IUserService UserService)
        {
            this.TestService = TestService;
            this.UserService = UserService;
        }

        [HttpGet]
        [Route("api/GetAllTest")]
        public async Task<IHttpActionResult> Index()
        {
            var temp = await TestService.GetAllTests();
            
            return Json(temp, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }

        [HttpGet]
        [Route("api/GetTest/{id}")]
        public IHttpActionResult GetTest(int id)
        {
            var test = TestService.GetTestById(id);
            return Json(test, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }

        [HttpGet]
        [Route("api/deleteTest/{id}")]
        public async Task<HttpResponseMessage> DeleteById(int id)
        {
            await TestService.DeleteTest(id);
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }

        [HttpPost]
        [Route("api/editTest")]
        public async Task<HttpResponseMessage> EditTest(Test model)
        {
            await TestService.EditTest(model);
            return new HttpResponseMessage(HttpStatusCode.Accepted) { Content = new StringContent(_EditTest) }; ;
        }

       
        [HttpPost]
        [Route("api/createTest")]
        public async Task<HttpResponseMessage> CreateTest(Test model)
        {
            //model.ApplicationUser = await UserService.FindByIdAsync(GetCurrentUserId());
            model.ApplicationUser = await UserService.FindByIdAsync("dae702a5-618f-4cda-971e-0551b79f5fb6");
            await UserService.CreateTest(model);
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }


        [HttpPost]
        [Route("api/login")]
        public async Task<HttpResponseMessage> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserService.FindByNameAsync(model.UserName);
                if (user != null)
                {

                    ApplicationUser appUser = new ApplicationUser { UserName = model.UserName };
                    ClaimsIdentity claim = await UserService.Authenticate(appUser, model.Password);

                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    return new HttpResponseMessage(HttpStatusCode.Accepted);
                }
                
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }
        private string GetCurrentUserId()
        {
            string userId = null;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    userId = userIdClaim.Value;
                }
            }
            return userId;
        }
    }
}
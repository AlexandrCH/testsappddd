﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TestsApp.Core;
using TestsApp.WEB.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;
using System;
using System.Security.Claims;

namespace TestsApp.WEB.Controllers
{
    [Authorize(Roles = ApplicationRoles.Administrator)]
    public class AdministrationController : Controller
    {
        #region Constants
        private const string Error = "Error occurred!!!";
        private const string CreatePasswordBody = "Please create password for new user by clicking ";
        private const string CreatePasswordSubject = "Create Password";
        private const string UserExist = "User with such email already exists";
        private const string RolesChanged = "!!! Your permissions are changed by Administrator! You will be logged out!";

        #endregion

        private IUserService UserService;
        private IManagerIdentity ManageService;
        private IEmailService EmailService;
        private ISignalR signalR;
        public AdministrationController(IUserService UserService, IManagerIdentity ManageService, IEmailService EmailService, ISignalR signalR)
        {
            this.UserService = UserService;
            this.ManageService = ManageService;
            this.EmailService = EmailService;
            this.signalR = signalR;
        }

        private IAuthenticationManager authenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult AddUsers()
        {
            AddUserModel model = new AddUserModel();
            model.Roles = new SelectList(ManageService.GetRoles(), "Name", "Name");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddUsers(AddUserModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserService.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        Email = model.Email,
                        UserName = model.Name,
                        FullName = model.FullName,
                        EmailConfirmed = true,
                        LockoutEnabled = true,
                        RegistrationDate = DateTime.Now.ToString()
                    };
                    var createUser = await UserService.CreateUser(user);
                    if (createUser.Succeeded)
                    {
                        await UserService.AddToRole(user.Id, ApplicationRoles.RegisteredUser);
                        await UserService.AddToRole(user.Id, model.Role);

                        await EmailService.SendAsync(CreatePasswordMessage(user));
                        return View("SuccessUserAdded");
                    }
                    ModelState.AddModelError(string.Empty, createUser.Errors.FirstOrDefault().ToString());
                    model.Roles = new SelectList(ManageService.GetRoles(), "Name", "Name");
                    return View(model);
                }
                model.Roles = new SelectList(ManageService.GetRoles(), "Name", "Name");
                ModelState.AddModelError(string.Empty, UserExist);
                return View(model);
            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> EditUsersProfile(string Id)
        {
            var user = await UserService.FindByIdAsync(Id);
            var editProfileModel = new EditProfileModel
            {
                Id = Id,
                Email = user.Email,
                Name = user.UserName,
                FullName = user.FullName,
                Roles = await UserService.GetRolesAsync(user.Id)
            };
            return View("UsersProfile", editProfileModel);
        }

        public ActionResult UsersProfile()
        {
            UsersModel users = new UsersModel
            {
                Users = new SelectList(ManageService.GetUsers(), "UserName", "UserName")
            };
            return View(users);
        }

        public ActionResult ManageUsers()
        {
            return View();
        }

        [HttpGet]
        public async Task<ContentResult> GetUserRoles(string Id)
        {
            var user = await UserService.FindByIdAsync(Id);
            var roles = await UserService.GetRolesAsync(user.Id);
            ManageRolesModel manageRoles = new ManageRolesModel
            {
                Roles = GetUsersRolesToList(ManageService.GetRoles(), roles)
            };
            var jsonData = JsonConvert.SerializeObject(manageRoles,
               Formatting.None,
               new JsonSerializerSettings()
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
            return Content(jsonData, "application/json");
        }

        [HttpPost]
        public async Task<HttpResponseMessage> ManageUsers(ManageRolesModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserService.AddRolesAsync(model.Id, model.SelectedRoles);
                if (result.Succedeed)
                {
                    signalR.NotifyUserAndLogout(model.UserName, string.Concat(model.UserName, RolesChanged));

                    return new HttpResponseMessage(HttpStatusCode.Accepted) ;
                }
            }
            return new HttpResponseMessage(HttpStatusCode.BadGateway);
        }

        public ContentResult GetCurrentUserId()
        {
            string userId = null;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    userId = userIdClaim.Value;
                }
            }
            var jsonData = JsonConvert.SerializeObject(userId,
                          Formatting.None,
                          new JsonSerializerSettings()
                          {
                              ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                          });
            return Content(jsonData, "application/json");
        }

        #region Private
        private IList<SelectListItem> GetUsersRolesToList(List<IdentityRole> allRoles, IList<string> userRoles)
        {
            var selectList = new List<SelectListItem>();
            foreach (var element in allRoles)
            {
                if (userRoles.Contains(element.Name))
                {
                    if (element.Name == ApplicationRoles.RegisteredUser)
                    {
                        selectList.Add(new SelectListItem
                        {
                            Value = element.Name,
                            Text = element.Name,
                            Selected = true,
                            Disabled = true
                        });
                    }
                    else
                    {
                        selectList.Add(new SelectListItem
                        {
                            Value = element.Name,
                            Text = element.Name,
                            Selected = true,
                            Disabled = false
                        });
                    }
                }
                else
                {
                    if (element.Name == ApplicationRoles.RegisteredUser)
                    {
                        selectList.Add(new SelectListItem
                        {
                            Value = element.Name,
                            Text = element.Name,
                            Selected = false,
                            Disabled = true
                        });
                    }
                    else
                    {
                        selectList.Add(new SelectListItem
                        {
                            Value = element.Name,
                            Text = element.Name,
                            Selected = false,
                            Disabled = false
                        });
                    }
                }
            }
            return selectList;
        }

        private IdentityMessage CreatePasswordMessage(ApplicationUser user)
        {
            var createPasswordUrl = $@"/Account/CreatePassword?userId={user.Id}";

            IdentityMessage message = new IdentityMessage
            {
                Body = CreatePasswordBody + " <a href=" + CustomConfigurationSection.Settings.BaseUrl + createPasswordUrl + ">here</a>",
                Subject = CreatePasswordSubject,
                Destination = user.Email
            };
            return message;
        }
        #endregion
    }
}
﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TestsApp.Core;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;
using TestsApp.WEB.Models;

namespace TestsApp.WEB.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        #region Constants
        private const string WrongPassword = "Password is wrong";
        private const string NoPassword = "Password is not created yet! Please check your email to create password";
        private const string NoSuchUser = "There is no such user";
        private const string Error = "Error occurred!!!";
        private const string ResetPasswordSubject = "Reset Password";
        private const string ResetPasswordBody = "Please reset your password by clicking ";
        private const string EmailNotExisted = "Email doesn't exist or you didn't confirm it yet!!!";
        private const string UserExist = "User with such email already exists";
        private const string ConfirmAccount = "Confirm your account";
        private const string ConfirmAccountBody = "Please confirm your account by clicking ";


        #endregion
        private IEmailService EmailService;
        private IUserService UserService;
        public AccountController(IUserService UserService, IEmailService EmailService)
        {
            this.UserService = UserService;
            this.EmailService = EmailService;
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserService.FindByNameAsync(model.UserName);
                if (user != null)
                {
                    if (user.PasswordHash == null)
                    {
                        ModelState.AddModelError(String.Empty, NoPassword);
                        return View(model);
                    }
                    if (user.LockoutEndDateUtc == null)
                    {
                        ApplicationUser appUser = new ApplicationUser { UserName = model.UserName };
                        ClaimsIdentity claim = await UserService.Authenticate(appUser, model.Password);
                        if (claim == null)
                        {
                            ModelState.AddModelError(String.Empty, WrongPassword);
                            return View(model);
                        }
                        else
                        {
                            AuthenticationManager.SignOut();
                            AuthenticationManager.SignIn(new AuthenticationProperties
                            {
                                IsPersistent = true
                            }, claim);
                            return RedirectToAction("Index", "Tests");
                        }
                    }
                    else
                    {
                        return View("Lockout");
                    }
                }
                ModelState.AddModelError(string.Empty, NoSuchUser);
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login", "Account");
        }
        [AllowAnonymous]

        public ActionResult NoPermission()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserService.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        Email = model.Email,
                        UserName = model.Name,
                        FullName = model.FullName,
                        RegistrationDate = DateTime.Now.ToString()
                    };
                    var createUser = await UserService.CreateUser(user, model.Password);
                    if (createUser.Succeeded)
                    {
                        await UserService.AddToRole(user.Id, ApplicationRoles.RegisteredUser);
                        var confirmationToken = await UserService.GenerateEmailConfirmation(user);
                        if (!string.IsNullOrEmpty(confirmationToken))
                        {
                            await EmailService.SendAsync(CreateConfirmAccountMessage(confirmationToken, user.Id, user.Email));
                            return View("SuccessRegister");
                        }
                        return View("Error");
                    }
                    ModelState.AddModelError(string.Empty, createUser.Errors.FirstOrDefault().ToString());
                    return View(model);
                }

                ModelState.AddModelError(string.Empty, UserExist);
                return View(model);
            }
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            var role = new string[] { ApplicationRoles.Student, ApplicationRoles.RegisteredUser };
            if (userId == null || code == null)
            {
                return View("Error");
            }
            OperationDetails result = await UserService.ConfirmEmail(userId, code);
            if (result.Succedeed)
            {
                await UserService.AddRolesAsync(userId, role);
                return View();
            }
            ModelState.AddModelError(result.Property, result.Message);
            return View("Error");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            var user = await UserService.FindByEmailAsync(model.Email);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, EmailNotExisted);
                return View(model);
            }
            var code = await UserService.GeneratePasswordResetCode(user);
            if (!string.IsNullOrEmpty(code))
            {
                await EmailService.SendAsync(CreateResetPasswordMessage(code, user.Id, model.Email));
                return View("ForgotPasswordConfirmation");
            }
            return View("Error");
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, Error);
                return View(model);
            }
            var result = await UserService.ResetPassword(model.Email, model.Code, model.Password);

            if (result.Succedeed)
            {
                return View("ResetPasswordConfirmation");
            }
            ModelState.AddModelError(string.Empty, result.Message);
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View("ChangePassword");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserService.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return View("SuccessPasswordChanged");
            }
            ModelState.AddModelError(string.Empty, result.Errors.FirstOrDefault());
            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult CreatePassword(string userId)
        {
            if (userId == null)
            {
                return View("Error");
            }
            CreatePasswordModel model = new CreatePasswordModel
            {
                Id = userId
            };
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> CreatePassword(CreatePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, Error);
                return View(model);
            }
            var result = await UserService.CreatePassword(model.Id, model.Password);

            if (result.Succedeed)
            {
                return View("CreatePasswordConfirmation");
            }
            ModelState.AddModelError(string.Empty, result.Message);
            return View();
        }

        public ActionResult EditProfile()
        {
            return View();
        }

        [HttpGet]
        public async Task<ContentResult> EditProfileGetData()
        {
            var userId = User.Identity.GetUserId();
            var user = await UserService.FindByIdAsync(userId);
            var model = new EditProfileModel
            {
                Id = userId,
                Email = user.Email,
                Name = User.Identity.GetUserName(),
                FullName = user.FullName,
                Roles = await UserService.GetRolesAsync(User.Identity.GetUserId()),
                Results = user.TestResults
            };
            var jsonData = JsonConvert.SerializeObject(model,
               Formatting.None,
               new JsonSerializerSettings()
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
            return Content(jsonData, "application/json");
        }

        #region Private
        private IdentityMessage CreateResetPasswordMessage(string code, string userID, string email)
        {
            code = System.Web.HttpUtility.UrlEncode(code);
            var resetPasswordUrl = $@"/Account/ResetPassword?userId={userID}&code={code}";

            IdentityMessage message = new IdentityMessage
            {
                Body = ResetPasswordBody + "<a href=" + CustomConfigurationSection.Settings.BaseUrl + resetPasswordUrl + ">here</a>",
                Subject = ResetPasswordSubject,
                Destination = email
            };
            return message;
        }
        private IdentityMessage CreateConfirmAccountMessage(string confirmationToken, string userID, string email)
        {
            confirmationToken = System.Web.HttpUtility.UrlEncode(confirmationToken);
            var confirmEmailUrl = $@"/Account/ConfirmEmail?userId={userID}&code={confirmationToken}";

            IdentityMessage message = new IdentityMessage
            {
                Body = ConfirmAccountBody + "<a href=" + CustomConfigurationSection.Settings.BaseUrl + confirmEmailUrl + ">here</a>",
                Subject = ConfirmAccount,
                Destination = email
            };
            return message;
        }

        #endregion
    }
}
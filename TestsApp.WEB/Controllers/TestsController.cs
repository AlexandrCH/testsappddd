﻿using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using TestsApp.Core.Interfaces;
using TestsApp.Core.Entities;
using Newtonsoft.Json;
using TestsApp.WEB.Models;
using TestsApp.Core;
using System.Linq;
using System.Threading.Tasks;
using TestsApp.Core.ViewModels;
using System.Security.Claims;
using System;
using System.IO;
using Microsoft.AspNet.Identity;

namespace TestsApp.WEB.Controllers
{
    [Authorize(Roles = "Student, Advanced Student, Tutor, Advanced Tutor, Administrator")]
    public class TestsController : Controller
    {
        private const string Subject = "Test results";
        private IUserService UserService;
        private ITestService TestService;
        private IManagerIdentity ManageService;
        private IEmailService EmailService;
        public TestsController( IUserService UserService, ITestService TestService, IManagerIdentity ManageService, IEmailService EmailService)
        {
            this.UserService = UserService;
            this.TestService = TestService;
            this.ManageService = ManageService;
            this.EmailService = EmailService;
        }

        public async Task<ActionResult> Index()
        {
            var tests = new TestsModel
            {
                AllTests = await TestService.GetAllTests(),
                UserId = GetCurrentUserId()
            };
            return View(tests);
        }

        [Authorize(Roles = "Tutor, Advanced Tutor")]
        [HttpGet]
        public ActionResult CreateTest()
        {
            return View();
        }

        [Authorize(Roles = "Tutor, Advanced Tutor")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateTest(Test model)
        {
            model.ApplicationUser = await UserService.FindByIdAsync(GetCurrentUserId());
            await UserService.CreateTest(model);
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }

        [Authorize(Roles = "Tutor, Advanced Tutor")]
        public ActionResult EditTest()
        {
            return View();
        }

        [Authorize(Roles = "Tutor, Advanced Tutor")]
        [HttpPost]
        public async Task<HttpResponseMessage> EditTest(Test model)
        {
            await TestService.EditTest(model);
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }

        [HttpGet]
        public ContentResult GetTest(int id)
        {
            var test = TestService.GetTestById(id);

            var jsonData = JsonConvert.SerializeObject(test,
                Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

            return Content(jsonData, "application/json");
        }

        [HttpGet]
        public ActionResult TestResultsModal(int id)
        {
            var test = TestService.GetTestById(id);

            if (test != null)
            {
                return PartialView(test);
            }
            return HttpNotFound();
        }

        [Authorize(Roles = "Tutor, Advanced Tutor")]
        [HttpGet]
        public async Task<ActionResult> DeleteTest(int id)
        {
            await TestService.DeleteTest(id);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Tutor, Advanced Tutor")]
        [HttpGet]
        public ActionResult DeleteTestModal(int id)
        {
            var test = TestService.GetTestById(id);

            if (test != null) {
                var entity = new TestsModel()
                {
                    Id = test.Id,
                    TestName = test.TestName
                };
                return PartialView(entity);
            }
            return HttpNotFound();
        }

        [Authorize(Roles = "Tutor, Advanced Tutor")]
        public ActionResult OfflineTest()
        {
            return View();
        }

        [Authorize(Roles = "Tutor, Advanced Tutor")]
        [HttpPost]
        public async Task<HttpResponseMessage> OfflineTest(TestViewModel model)
        {
            await TestService.CreateTestResult(TestService.CalculateResult(model), model);
            var user = await UserService.FindByIdAsync(model.ApplicationUserId);
            var entity = new TestResultViewModel
            {
                appUser = user,
                date = DateTime.Now.ToString(),
                test = TestService.GetTestById(model.TestId),
                testResult = TestService.CalculateResult(model)
            };

            var htmlTable = RenderPartialViewToString("TestResultTable", entity);
            IdentityMessage message = new IdentityMessage
            {
                Body = htmlTable,
                Subject = Subject,
                Destination = user.Email
            };
            await EmailService.SendAsync(message);
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }


        [HttpGet]
        public async Task<ContentResult> GetAllTests()
        {
            var test = await TestService.GetAllTests();
            var jsonData = JsonConvert.SerializeObject(test,
               Formatting.None,
               new JsonSerializerSettings()
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
            return Content(jsonData, "application/json");
        }

        [AllowAnonymous]
        [HttpGet]
        public ContentResult GetUsers()
        {
            var users = ManageService.GetUsers();
            var jsonData = JsonConvert.SerializeObject(users,
               Formatting.None,
               new JsonSerializerSettings()
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
            return Content(jsonData, "application/json");
        }

        public ActionResult OnlineTest()
        {
            return View();
        }

        public ContentResult GetTestById(int id)
        {
            var test = TestService.GetTestById(id);
            var jsonData = JsonConvert.SerializeObject(test,
                          Formatting.None,
                          new JsonSerializerSettings()
                          {
                              ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                          });
            return Content(jsonData, "application/json");
        }

        public ContentResult GetCurrentUser()
        {
            string userId = null;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    userId = userIdClaim.Value;
                }
            }
            var jsonData = JsonConvert.SerializeObject(userId,
                          Formatting.None,
                          new JsonSerializerSettings()
                          {
                              ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                          });
            return Content(jsonData, "application/json");
        }

        [HttpPost]
        public async Task<HttpResponseMessage> OnlineTest(TestViewModel model)
        {
            await TestService.CreateTestResult(TestService.CalculateResult(model), model);
            var user = await UserService.FindByIdAsync(model.ApplicationUserId);
            var entity = new TestResultViewModel
            {
                appUser = user,
                date = DateTime.Now.ToString(),
                test = TestService.GetTestById(model.TestId),
                testResult = TestService.CalculateResult(model)
            };

            var htmlTable = RenderPartialViewToString("TestResultTable", entity);
            IdentityMessage message = new IdentityMessage
            {
                Body = htmlTable,
                Subject = Subject,
                Destination = user.Email
            };
            await EmailService.SendAsync(message);
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }

        #region Private
        private string GetCurrentUserId()
        {
            string userId = null;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                var userIdClaim = claimsIdentity.Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim != null)
                {
                    userId = userIdClaim.Value;
                }
            }
            return userId;
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        #endregion
    }
}
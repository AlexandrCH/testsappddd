﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace TestsApp.WEB
{
    public static class WebApiConfig
    {
        /// <summary>
        /// Configuration for WebApi <see cref="HttpConfiguration"/>
        /// </summary>
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            //turn off send in XML format
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            //eanable CORS
            config.EnableCors();
        }
    }
}
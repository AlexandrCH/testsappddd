﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestsApp.Core;
using TestsApp.IoC;

[assembly: OwinStartup(typeof(TestsApp.WEB.App_Start.Startup))]

namespace TestsApp.WEB.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/NoPermission"),
                ExpireTimeSpan = TimeSpan.FromMinutes(10),
                SlidingExpiration = true
            });
            app.MapSignalR();
        }
    }
}
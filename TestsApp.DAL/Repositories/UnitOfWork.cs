﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System.Configuration;
using System.Threading.Tasks;
using TestsApp.Core;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;
using TestsApp.DAL.EntityFramework;

namespace TestsApp.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationContext context;

        private GenericRepository<Test> tests;
        private GenericRepository<TestResult> testResults;

        private UserManager<ApplicationUser> userManager;
        private RoleManager<IdentityRole> roleManager;

        public UnitOfWork(string connectionString)
        {
            context = new ApplicationContext(connectionString);
            roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            userManager = ApplicationUserManager.Create(context);
        }


        public UserManager<ApplicationUser> UserManager
        {
            get { return userManager; }
        }

        public RoleManager<IdentityRole> RoleManager
        {
            get { return roleManager; }
        }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        public IGenericRepository<Test> Tests
        {
            get
            {
                if (tests == null)
                    tests = new GenericRepository<Test>(context);
                return tests;
            }
        }

        public IGenericRepository<TestResult> TestResults
        {
            get
            {
                if (testResults == null)
                testResults = new GenericRepository<TestResult>(context);
                return testResults;
            }
        }
    }
}
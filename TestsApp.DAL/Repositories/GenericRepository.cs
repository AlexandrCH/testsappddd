﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;
using TestsApp.DAL.EntityFramework;
using RefactorThis.GraphDiff;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq.Expressions;

namespace TestsApp.DAL.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly ApplicationContext context;
        private IDbSet<T> dbEntity = null;

        public GenericRepository(ApplicationContext context)
        {
            this.context = context;
            dbEntity = this.context.Set<T>();
        }

        public async Task Create(T entity)
        {
            dbEntity.Add(entity);
            await context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var test = dbEntity.Find(id);
            dbEntity.Remove(test);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await dbEntity.ToListAsync();
        }

        public T GetById(int id)
        {
            return  dbEntity.Find(id);
        }

        public async Task<T> GetById2(int id)
        {
            return await dbEntity.FirstOrDefaultAsync(x => x.Id == id);
        }
        public async Task Update(T entity)
        {
            //db.UpdateGraph(entity, map => map.OwnedCollection(r => r.Questions, with => with.OwnedCollection(p => p.Answers)));
            //await db.SaveChangesAsync();


            //dbEntity.Attach(entity);
            //context.Entry(entity).State = EntityState.Modified;

            context.Entry(entity).State = EntityState.Modified;

            //var exist = await context.Set<T>().FindAsync(entity.id);
            //context.Entry(exist).CurrentValues.SetValues(entity);


            await context.SaveChangesAsync();
        }
    }
}
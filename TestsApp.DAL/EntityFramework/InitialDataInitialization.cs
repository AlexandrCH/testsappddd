﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestsApp.Core;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;

namespace TestsApp.DAL.EntityFramework
{
    public class InitialDataInitialization : CreateDatabaseIfNotExists<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            #region Constants
            const string Password = "qqqqqq";
            const string Email = "testalexac@gmail.com";
            const string UserName = "admin";
            const string FullName = "Test Full Name";
            #endregion
            var store = new RoleStore<IdentityRole>(context);
            var manager = new RoleManager<IdentityRole>(store);

            List<IdentityRole> identityRoles = new List<IdentityRole>();
            identityRoles.Add(new IdentityRole() { Name = ApplicationRoles.RegisteredUser });
            identityRoles.Add(new IdentityRole() { Name = ApplicationRoles.Student });
            identityRoles.Add(new IdentityRole() { Name = ApplicationRoles.AdvancedStudent });
            identityRoles.Add(new IdentityRole() { Name = ApplicationRoles.Tutor });
            identityRoles.Add(new IdentityRole() { Name = ApplicationRoles.AdvancedTutor });
            identityRoles.Add(new IdentityRole() { Name = ApplicationRoles.Administrator });

            foreach (IdentityRole role in identityRoles)
            {
                manager.Create(role);
            }

            var userStore = new UserStore<ApplicationUser>(context);
            var userManage = new UserManager<ApplicationUser>(userStore);
            ApplicationUser user = new ApplicationUser
            {
                Email = Email,
                UserName = UserName,
                FullName = FullName,
                EmailConfirmed = true,
                RegistrationDate = DateTime.Now.ToString()
            };

            userManage.Create(user, Password);
            userManage.AddToRole(user.Id, ApplicationRoles.RegisteredUser);
            userManage.AddToRole(user.Id, ApplicationRoles.Administrator);
        }
    }
}

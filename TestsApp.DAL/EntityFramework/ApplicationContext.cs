﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using TestsApp.Core;
using TestsApp.Core.Entities;

namespace TestsApp.DAL.EntityFramework
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext(string connectionString) : base(connectionString)
        {
            Database.SetInitializer(new InitialDataInitialization());
        }

        public DbSet<Test> TestsDB { get; set; }
        public DbSet<Question> QuestionsDB { get; set; }
        public DbSet<Answer> AnswersDB { get; set; }
        public DbSet<TestResult> TestResultDB { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Test>()
                .HasMany(t => t.Questions)
                .WithOptional(s => s.Test)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Question>()
               .HasMany(t => t.Answers)
               .WithOptional(c => c.Questions)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<Answer>()
                .Ignore(x=>x.IsChecked)
                .HasOptional(x => x.Questions)
                .WithMany(x => x.Answers)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<TestResult>()
               .HasOptional(x => x.Test)
               .WithMany(x => x.TestResults)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<TestResult>()
               .HasOptional(x => x.ApplicationUser)
               .WithMany(x => x.TestResults)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<Test>().HasKey(s => s.Id);
            modelBuilder.Entity<Test>().ToTable("Tests");
            modelBuilder.Entity<Question>().ToTable("Questions");
            modelBuilder.Entity<Answer>().ToTable("Answers");
            modelBuilder.Entity<TestResult>().ToTable("TestResults");
            modelBuilder.Entity<IdentityUserLogin>().HasKey(x => x.UserId);
            modelBuilder.Entity<IdentityUserRole>().HasKey(x => new { x.UserId, x.RoleId }).ToTable("AspNetUserRoles");

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>()
                 .Ignore(c => c.AccessFailedCount)
                 .Ignore(c => c.PhoneNumber)
                 .Ignore(c => c.PhoneNumberConfirmed)
                 .Ignore(c => c.TwoFactorEnabled);
        }
    }
}
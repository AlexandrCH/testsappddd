﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsApp.Core
{
    public class CustomConfigurationSection : ConfigurationSection
    {
        private static CustomConfigurationSection settings = ConfigurationManager.GetSection("CustomConfigurationSection") as CustomConfigurationSection;

        public static CustomConfigurationSection Settings
        {
            get
            {
                return settings;
            }
        }

        [ConfigurationProperty("BaseUrl")]
        public string BaseUrl
        {
            get { return (string)this["BaseUrl"]; }
            set { this["BaseUrl"] = value; }
        }

        [ConfigurationProperty("Email")]
        public string Email
        {
            get { return (string)this["Email"]; }
            set { this["Email"] = value; }
        }

        [ConfigurationProperty("Password")]
        public string Password
        {
            get { return (string)this["Password"]; }
            set { this["Password"] = value; }
        }

        [ConfigurationProperty("SmtpHost")]
        public string SmtpHost
        {
            get { return (string)this["SmtpHost"]; }
            set { this["SmtpHost"] = value; }
        }

        [ConfigurationProperty("SmtpPort")]
        public string SmtpPort
        {
            get { return (string)this["SmtpPort"]; }
            set { this["SmtpPort"] = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestsApp.Core;
using TestsApp.Core.Entities;

namespace TestsApp.Core.ViewModels
{
    public class TestViewModel
    {
        public virtual ICollection<Question> Questions { get; set; }
        public int TestId { get; set; }
        public string ApplicationUserId { get; set; }
    }
}
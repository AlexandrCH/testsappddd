﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestsApp.Core;
using TestsApp.Core.Entities;

namespace TestsApp.Core.ViewModels
{
    public class TestResultViewModel
    {
        public string date { get; set; }
        public Test test { get; set; }
        public ApplicationUser appUser { get; set; }
        public int testResult { get; set; }
    }
}
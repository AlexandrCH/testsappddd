﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TestsApp.Core.Entities
{
    public class Answer : BaseEntity
    {
        public string AnswerName { get; set; }

        public bool IsCorrect { get; set; }

        public bool IsChecked { get; set; }

        public virtual Question Questions { get; set; }
    }
}
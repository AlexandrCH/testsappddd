﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestsApp.Core.Entities
{
    public class TestResult : BaseEntity
    {
        public int Result { get; set; }
        public string Date { get; set; }
        public virtual Test Test { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
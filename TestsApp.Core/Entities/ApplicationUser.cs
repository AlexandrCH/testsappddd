﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TestsApp.Core.Entities;

namespace TestsApp.Core.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
        public string RegistrationDate { get; set; }
        public virtual ICollection<TestResult> TestResults { get; set; }
        public virtual ICollection<Test> Tests { get; set; }
    }
}
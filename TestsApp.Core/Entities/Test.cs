﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestsApp.Core.Entities
{
    public class Test : BaseEntity
    {
        [Required]
        public string TestName { get; set; }

        public string Information { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<TestResult> TestResults { get; set; }
    }
}
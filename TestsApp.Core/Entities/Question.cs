﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestsApp.Core.Entities
{
    public class Question : BaseEntity
    {
        public string QuestionName { get; set; }

        public virtual Test Test { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;
using TestsApp.Core.ViewModels;

namespace TestsApp.Core.Interfaces
{
    public interface ITestService
    {
        int CalculateResult(TestViewModel entity);
        Task CreateTestResult(int result, TestViewModel entity);
        Task <IEnumerable<Test>> GetAllTests();
        Test GetTestById(int id);
        Task DeleteTest(int id);
        Task EditTest(Test entity);
        Task<Test> GetTestById2(int id);
    }
}
﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using TestsApp.Core.Entities;

namespace TestsApp.Core.Interfaces
{
    public interface IUserService 
    {
        Task<IdentityResult> CreateUser(ApplicationUser appUser);
        Task<IdentityResult> CreateUser(ApplicationUser appUser, string password);
        Task<ClaimsIdentity> Authenticate(ApplicationUser appUser, string password);
        Task<OperationDetails> ConfirmEmail(string userId, string code);
        Task<string> GeneratePasswordResetCode(ApplicationUser user);
        Task<string> GenerateEmailConfirmation(ApplicationUser user);
        Task<OperationDetails> ResetPassword(string email, string code, string password);
        Task<OperationDetails> CreatePassword(string userId, string password);
        Task<OperationDetails> AddRolesAsync(string userId, string[] roles);
        Task<ApplicationUser> FindByIdAsync(string userId);
        Task<ApplicationUser> FindByEmailAsync(string email);
        Task<List<string>> GetRolesAsync(string userId);
        Task<IdentityResult> UpdateUserAsync(ApplicationUser appUser);
        Task CreateTest(Test entity);
        Task<IdentityResult> ChangePasswordAsync(string userId, string oldPassword, string newPassword);
        Task<ApplicationUser> FindByNameAsync(string userName);
        Task AddToRole(string userId, string role);
    }
}
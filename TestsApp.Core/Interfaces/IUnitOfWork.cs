﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using TestsApp.Core.Entities;

namespace TestsApp.Core.Interfaces
{
    public interface IUnitOfWork
    {
        IGenericRepository<Test> Tests { get; }
        IGenericRepository<TestResult> TestResults { get; }
        UserManager<ApplicationUser> UserManager { get; }
        RoleManager<IdentityRole> RoleManager { get; }
        Task SaveAsync();
    }
}
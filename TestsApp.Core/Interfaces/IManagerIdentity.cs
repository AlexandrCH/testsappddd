﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using TestsApp.Core.Entities;

namespace TestsApp.Core.Interfaces
{
    public interface IManagerIdentity
    {
        List<IdentityRole> GetRoles();
        List<ApplicationUser> GetUsers();
    }
}
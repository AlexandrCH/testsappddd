﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsApp.Core.Interfaces
{
    public interface IEmailService
    {
        Task SendAsync(IdentityMessage message);
        void Send(IdentityMessage message);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TestsApp.Core.Entities;

namespace TestsApp.Core.Interfaces
{
    public interface IGenericRepository<T>  where T : BaseEntity
    {
        Task Create(T entity);
        Task Update(T entity);
        Task Delete(int id);
        Task<IEnumerable<T>> GetAll();
        T GetById(int id);
        Task<T> GetById2(int id);
    }
}
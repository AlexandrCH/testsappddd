﻿namespace TestsApp.Core
{
    public static class ApplicationRoles
    {
        public const string RegisteredUser = "Registered User";
        public const string Student = "Student";
        public const string AdvancedStudent = "Advanced Student";
        public const string Tutor = "Tutor";
        public const string AdvancedTutor = "Advanced Tutor";
        public const string Administrator = "Administrator";
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestsApp.WEB.Controllers;
using System.Web.Mvc;
using System.Threading.Tasks;
using Moq;
using TestsApp.Core;
using TestsApp.WEB.Models;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using TestsApp.Core.Interfaces;

namespace TestsApp.Tests.Controllers
{
    [TestClass]
    public class AdministrationControllerTest
    {
      
        [TestMethod]
        public void Index()
        {
            AdministrationController controller = new AdministrationController(null, null, null, null);
            Task<ActionResult> result = controller.EditUsersProfile("") as Task<ActionResult>;
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void AddUsers()
        {
            Mock<IUserService> mockUserTasks = new Mock<IUserService>();
            Mock<AddUserModel> adduser = new Mock<AddUserModel>();
            adduser.Setup(x => x.Roles == new SelectList(new List<IdentityRole>()));
            AdministrationController controller = new AdministrationController(mockUserTasks.Object, null, null, null);
            ViewResult result = controller.AddUsers() as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}
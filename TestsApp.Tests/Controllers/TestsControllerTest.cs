﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;
using TestsApp.DAL.Repositories;
using TestsApp.WEB.Controllers;

namespace TestsApp.Tests.Controllers
{
    [TestClass]
    public class TestsControllerTest
    {
        Test test1 = null;
        Test test2 = null;
        Test test3 = null;
        Test test4 = null;
        Test test5 = null;

        List<Test> tests = null;
        //StubTestRepository testsRepo = null;
        //UnitOfWork uow = null;
        //TestsController controller = null;

        public TestsControllerTest()
        {
            test1 = new Test { TestName = "test1", Information = "info1" };
            test2 = new Test { TestName = "test2", Information = "info2" };
            test3 = new Test { TestName = "test3", Information = "info3" };
            test4 = new Test { TestName = "test4", Information = "info4" };
            test5 = new Test { TestName = "test5", Information = "info5" };

            tests = new List<Test>
            {
                test1,
                test2,
                test3,
                test4,
                test5
            };

            //testsRepo = new StubTestRepository(tests);
            //uow = new EFUnitOfWork(testsRepo);
            //controller = new TestsController(uow);
        }

        //[TestMethod]
        //public async Task Verify_That_Test_Is_Updated()
        //{
        //    Test test = new Test { Id = 456, TestName = "test5555", Information = "info678678678" };
        //    await controller.EditTest(test);
        //    var tests = await testsRepo.GetAll();
        //    CollectionAssert.Contains(tests, test);
        //}

        //[TestMethod]
        //public async Task Verify_Find_Test_ByID_Returns_Approppriate_Test()
        //{
        //    ViewResult result = await controller.TestResults(2) as ViewResult;
        //    Assert.AreEqual(result.Model, test3);
        //}

        //[TestMethod]
        //public async Task Verify_That_Approppriate_Test_Is_Deleted()
        //{
        //    await controller.DeleteTest(1);
        //    //var tests = await testsRepo.GetAll();
        //    //CollectionAssert.DoesNotContain(tests, test2);
        //}
    }
}
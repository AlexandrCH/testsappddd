﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestsApp.WEB.Controllers;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Moq;

namespace TestsApp.Tests.Controllers
{
    [TestClass]
    public class AccountControllerTest
    {
        [TestMethod]
        public void Login()
        {
            AccountController controller = new AccountController(null, null);

            ViewResult result = controller.Login() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Register()
        {
            AccountController controller = new AccountController(null, null);

            ViewResult result = controller.Register() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ConfirmEmail()
        {
            AccountController controller = new AccountController(null, null);

            var result = controller.ConfirmEmail("Id", "Code");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ForgotPassword()
        {
            AccountController controller = new AccountController(null, null);

            var result = controller.ForgotPassword() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ResetPassword()
        {
            AccountController controller = new AccountController(null, null);

            var result = controller.ResetPassword("Code") as ViewResult;

            Assert.IsNotNull(result);
        }
    }
}
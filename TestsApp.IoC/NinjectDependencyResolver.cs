﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestsApp.BLL;
using TestsApp.Core;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;
using TestsApp.DAL;
using TestsApp.DAL.Repositories;
using TestsApp.BLL.SignalR;
using TestsApp.BLL.SignalR.Hubs;

namespace TestsApp.IoC
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            kernel.Bind<ISignalR>().To<NotificationHub>();
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IGenericRepository<Test>>().To<GenericRepository<Test>>();
            kernel.Bind<IGenericRepository<TestResult>>().To<GenericRepository<TestResult>>();
            kernel.Bind<IEmailService>().To<EmailService>();
            kernel.Bind<ITestService>().To<TestService>();
            kernel.Bind<IManagerIdentity>().To<ManagerIdentity>();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument("DefaultConnection");
        }
    }
}
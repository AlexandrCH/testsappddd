﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestsApp.BLL;

namespace TestsApp.BLL
{
    public class JobScheduler
    {
        public async static void Start()
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();

            IJobDetail job = JobBuilder.Create<DeleteUserJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                //.WithSimpleSchedule(s => s.WithIntervalInSeconds(20).RepeatForever()).Build();
                .WithDailyTimeIntervalSchedule
                  (s =>
                     s.WithIntervalInHours(24)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(10, 30))
                  )
                .Build();

            await scheduler.ScheduleJob(job, trigger);
        }
    }
}

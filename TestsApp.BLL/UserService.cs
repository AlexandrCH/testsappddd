﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TestsApp.Core;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;

namespace TestsApp.BLL
{
    public class UserService : IUserService
    {
        #region constants
        private const string ConfirmAccount = "Confirm your account";
        private const string SuccessRegistration = "Registration is successed";
        private const string UserExist = "User with such email already exists";
        private const string ConfirmFailed = "Confirmation is failed! 24 hours passed. Please sign up again and confirm email!";
        private const string EmailNotExisted = "Email doesn't exist or you didn't confirm it yet!!!";
        private const string ResetPasswordBody = "Please reset your password by clicking ";
        private const string ConfirmAccountBody = "Please confirm your account by clicking ";
        private const string NoEmail = "There is no such email";
        private const string CreatePasswordBody = "Please create password for new user by clicking ";
        private const string NoSuchUser = "There is no such user!";
        private const string CreatePasswordSubject = "Create Password";
        private const string ResetPasswordSubject = "Reset Password";
        private const string OkMessage = "Ok!";
        #endregion

        private IUnitOfWork unitOfWork;
        private IEmailService emailService;
        public UserService(IUnitOfWork unitOfWork, IEmailService emailService)
        {
            this.unitOfWork = unitOfWork;
            this.emailService = emailService;
        }

        public async Task<IdentityResult> CreateUser(ApplicationUser appUser)
        {
            return await unitOfWork.UserManager.CreateAsync(appUser);
        }
        public async Task<IdentityResult> CreateUser(ApplicationUser appUser, string password)
        {
            return await unitOfWork.UserManager.CreateAsync(appUser, password);
        }
        public async Task AddToRole(string userId, string role)
        {
            await unitOfWork.UserManager.AddToRoleAsync(userId, role);
            await unitOfWork.SaveAsync();
        }
        public async Task<string> GenerateEmailConfirmation(ApplicationUser user)
        {
            return await unitOfWork.UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
        }

        public async Task<ClaimsIdentity> Authenticate(ApplicationUser appUser, string password)
        {
            ClaimsIdentity claim = null;
            ApplicationUser user = await unitOfWork.UserManager.FindAsync(appUser.UserName, password);
            if (user != null)
                claim = await unitOfWork.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

        public async Task<OperationDetails> ConfirmEmail(string userId, string code)
        {
            var resultFindUser = await unitOfWork.UserManager.FindByIdAsync(userId);
            if (resultFindUser == null)
            {
                return new OperationDetails(false, ConfirmFailed, string.Empty);
            }
            var result = await unitOfWork.UserManager.ConfirmEmailAsync(userId, code);
            if (result.Errors.Count() > 0)
            {
                return new OperationDetails(false, result.Errors.FirstOrDefault(), string.Empty);
            }
            var rolesForUser = await unitOfWork.UserManager.GetRolesAsync(userId);
            unitOfWork.UserManager.AddToRole(userId, ApplicationRoles.Student);
            return new OperationDetails(true, OkMessage, string.Empty);
        }

        public async Task<ApplicationUser> FindByNameAsync(string userName)
        {
            return await unitOfWork.UserManager.FindByNameAsync(userName);
        }

        public async Task<List<string>> GetRolesAsync(string userId)
        {
            return (List<string>)await unitOfWork.UserManager.GetRolesAsync(userId);
        }

        public async Task<IdentityResult> UpdateUserAsync(ApplicationUser appUser)
        {
            return await unitOfWork.UserManager.UpdateAsync(appUser);
        }

        public async Task<IdentityResult> ChangePasswordAsync(string userId, string oldPassword, string newPassword)
        {
            return await unitOfWork.UserManager.ChangePasswordAsync(userId, oldPassword, newPassword);
        }

        public async Task<string> GeneratePasswordResetCode(ApplicationUser user)
        {
            if (user == null || !(await unitOfWork.UserManager.IsEmailConfirmedAsync(user.Id)))
            {
                return null;
            }
            return await unitOfWork.UserManager.GeneratePasswordResetTokenAsync(user.Id);
        }
        public async Task<OperationDetails> ResetPassword(string email, string code, string password)
        {
            var user = await unitOfWork.UserManager.FindByEmailAsync(email);
            if (user == null)
            {
                return new OperationDetails(false, NoEmail, string.Empty);
            }

            var result = await unitOfWork.UserManager.ResetPasswordAsync(user.Id, code, password);
            if (result.Errors.Count() > 0)
            {
                return new OperationDetails(false, result.Errors.FirstOrDefault(), string.Empty);
            }
            return new OperationDetails(true, OkMessage, string.Empty);
        }

        public async Task<OperationDetails> AddRolesAsync(string userId, string[] roles)
        {
            var userRoles = await unitOfWork.UserManager.GetRolesAsync(userId);
            foreach (var role in userRoles)
            {
                await unitOfWork.UserManager.RemoveFromRoleAsync(userId, role);
            }
            var result = await unitOfWork.UserManager.AddToRolesAsync(userId, roles);
            if (result.Errors.Count() > 0)
            {
                return new OperationDetails(false, result.Errors.FirstOrDefault(), string.Empty);
            }

            return new OperationDetails(true, OkMessage, string.Empty);
        }

        public async Task<OperationDetails> CreatePassword(string userId, string password)
        {
            var user = await unitOfWork.UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                return new OperationDetails(false, NoSuchUser, string.Empty);
            }
            var create = await unitOfWork.UserManager.AddPasswordAsync(userId, password);
            if (create.Errors.Count() > 0)
            {
                return new OperationDetails(false, create.Errors.FirstOrDefault(), string.Empty);
            }

            return new OperationDetails(true, OkMessage, string.Empty);
        }

        public async Task<ApplicationUser> FindByIdAsync(string userId)
        {
            return await unitOfWork.UserManager.FindByIdAsync(userId);
        }

        public Task CreateTest(Test entity)
        {
            return unitOfWork.Tests.Create(entity);
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email)
        {
            return await unitOfWork.UserManager.FindByEmailAsync(email);
        }
    }
}
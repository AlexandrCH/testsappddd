﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestsApp.Core;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;
using TestsApp.Core.ViewModels;

namespace TestsApp.BLL
{
    public class TestService : ITestService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IEmailService emailService;
        public TestService(IUnitOfWork unitOfWork, IEmailService emailService)
        {
            this.unitOfWork = unitOfWork;
            this.emailService = emailService;
        }

        public int CalculateResult(TestViewModel entity)
        {
            var wrongAnsweredQuestions = entity.Questions.Where(x => x.Answers.Any(y => y.IsCorrect == false & y.IsChecked == true || y.IsCorrect == true & y.IsChecked == false)).Count();
            var totalQuestions = entity.Questions.Where(x => x.Answers.Any(y => y.IsCorrect == true)).Count();
            int result = 100 - 100 * wrongAnsweredQuestions / totalQuestions;
            return result;
        }

        public async Task CreateTestResult(int result, TestViewModel entity)
        {
            var test = unitOfWork.Tests.GetById(entity.TestId);
            var user = await unitOfWork.UserManager.FindByIdAsync(entity.ApplicationUserId);

            var testResult = new TestResult
            {
                Date = DateTime.Now.ToString(),
                Result = result,
                Test = test,
                ApplicationUser = user
            };
            await unitOfWork.TestResults.Create(testResult);
        }

        public async Task<IEnumerable<Test>> GetAllTests()
        {
            return await unitOfWork.Tests.GetAll();
        }

        public Test GetTestById(int id)
        {
            return unitOfWork.Tests.GetById(id);
        }
        public async Task<Test> GetTestById2(int id)
        {
            return await unitOfWork.Tests.GetById2(id);
        }
        public async Task DeleteTest(int id)
        {
            await unitOfWork.Tests.Delete(id);
        }

        public async Task EditTest(Test entity)
        {
            await unitOfWork.Tests.Update(entity);
        }
    }
}
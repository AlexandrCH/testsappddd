﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using TestsApp.DAL.EntityFramework;

namespace TestsApp.BLL
{
    public class DeleteUsersService
    { 
        /// <summary>
        /// To delete users that didn't confirme email registration after 24 hours
        /// </summary>
        public void DeleteNotConfirmedUsers()
        {
            var timer = new Timer();
            //check every 1 hour
            timer.Interval = TimeSpan.FromHours(1).TotalMilliseconds;
            timer.Elapsed += HandlerTimerElapsed;
            timer.Start();
        }
        public static DeleteUsersService Create()
        {
            return new DeleteUsersService();
        }

        private async void HandlerTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Debug.WriteLine("DeleteNotConfirmedUsers " + DateTime.Now);
            using (var db = new ApplicationContext("DefaultConnection"))
            {
                var users = db.Users.Where(x => x.EmailConfirmed == false);
                if (users.Count() != 0)
                {
                    foreach (var item in users)
                    {
                        var currentTime = DateTime.Now;
                        var registrationDate = DateTime.Parse(item.RegistrationDate);
                        if (currentTime > registrationDate.AddDays(1))
                        {
                            using (var dbConnection = new ApplicationContext("DefaultConnection"))
                            {
                                var user = dbConnection.Users.FirstOrDefault(c => c.Id == item.Id);
                                dbConnection.Users.Remove(user);
                                await dbConnection.SaveChangesAsync();
                            }
                        }
                    }
                }
            }
        }
    }
}
﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TestsApp.Core;
using TestsApp.Core.Entities;
using TestsApp.Core.Interfaces;

namespace TestsApp.BLL
{
    public class ManagerIdentity : IManagerIdentity
    {

        private IUnitOfWork unitOfWork;
        public ManagerIdentity(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public List<IdentityRole> GetRoles()
        {
            return unitOfWork.RoleManager.Roles.ToList();
        }

        public List<ApplicationUser> GetUsers()
        {
            return unitOfWork.UserManager.Users.ToList();
        }
    }
}
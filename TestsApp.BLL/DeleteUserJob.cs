﻿using Ninject;
using Ninject.Syntax;
using Quartz;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestsApp.Core.Interfaces;
using TestsApp.DAL.EntityFramework;

namespace TestsApp.BLL
{
    public class DeleteUserJob : IJob
    {

        #region Ninject
        //private IUnitOfWork unitOfWork;
        //public DeleteUserlJob(IUnitOfWork unitOfWork)
        //{
        //    this.unitOfWork = unitOfWork;
        //}

        //Debug.Print("Teeeeeeeeeeeeeest");
        //var users = unitOfWork.UserManager.Users.Where(x => x.EmailConfirmed == false);
        //if (users.Count() != 0)
        //{
        //    foreach (var item in users)
        //    {
        //        var currentTime = DateTime.Now;
        //        var registrationDate = DateTime.Parse(item.RegistrationDate);
        //        if (currentTime > registrationDate.AddDays(1))
        //        {

        //            var user = unitOfWork.UserManager.Users.FirstOrDefault(c => c.Id == item.Id);
        //            await unitOfWork.UserManager.DeleteAsync(user);
        //            await unitOfWork.SaveAsync();
        //        }
        //    }
        //}

        #endregion

        public async Task Execute(IJobExecutionContext context)
        {
            Debug.WriteLine("DeleteNotConfirmedUsers " + DateTime.Now);
            using (var db = new ApplicationContext("DefaultConnection"))
            {
                var users = db.Users.Where(x => x.EmailConfirmed == false);
                if (users.Count() != 0)
                {
                    foreach (var item in users)
                    {
                        var currentTime = DateTime.Now;
                        var registrationDate = DateTime.Parse(item.RegistrationDate);
                        if (currentTime > registrationDate.AddDays(1))
                        {
                            using (var dbConnection = new ApplicationContext("DefaultConnection"))
                            {
                                var user = dbConnection.Users.FirstOrDefault(c => c.Id == item.Id);
                                dbConnection.Users.Remove(user);
                                await dbConnection.SaveChangesAsync();
                            }
                        }
                    }
                }
            }
        }
    }
}

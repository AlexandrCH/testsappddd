﻿using Microsoft.AspNet.Identity;
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using TestsApp.Core;
using TestsApp.Core.Interfaces;

namespace TestsApp.BLL
{
    public class EmailService : IIdentityMessageService, IEmailService
    {
        public Task SendAsync(IdentityMessage message)
        {
            return Task.Factory.StartNew(() =>
            {
                CreateMessage(message).SendMessage();
            });
        }
        public void Send(IdentityMessage message)
        {
            CreateMessage(message).SendMessage();
        }

        public MailMessage CreateMessage(IdentityMessage message)
        {
            MailMessage msg = new MailMessage();
            msg.IsBodyHtml = true;
            msg.From = new MailAddress(CustomConfigurationSection.Settings.Email);
            msg.To.Add(new MailAddress(message.Destination));
            msg.Subject = message.Subject;
            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(message.Body, null, MediaTypeNames.Text.Plain));
            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(message.Body, null, MediaTypeNames.Text.Html));
            return msg;
        }
    }

    public static class ExtentionMailMessage
    {
        public static void SendMessage(this MailMessage msg)
        {
            var smtpClient = new SmtpClient(CustomConfigurationSection.Settings.SmtpHost, Convert.ToInt32(CustomConfigurationSection.Settings.SmtpPort));
            var credentials = new NetworkCredential(CustomConfigurationSection.Settings.Email, CustomConfigurationSection.Settings.Password);
            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);
        }
    }
}
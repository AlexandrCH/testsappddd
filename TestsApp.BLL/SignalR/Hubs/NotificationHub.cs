﻿using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using TestsApp.Core.Interfaces;

namespace TestsApp.BLL.SignalR.Hubs
{
    public class NotificationHub : Hub, ISignalR
    {
        public void NotifyUserAndLogout(string name, string message)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            context.Clients.Group(name).notifyUserAndLogout( message);
        }

        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;
            Groups.Add(Context.ConnectionId, name);
            return base.OnConnected();
        }
    }
}